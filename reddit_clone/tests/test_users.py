import pytest
from jose import jwt
from dbmgt_orm.utilities import jwt_manager

# test the creation of a user
# ex: is the status code returned 201
def test_create_user(client):
    res = client.post("/users", json = {
        "email":"user@something.lu",
        "password": "4321"} )
    assert res.status_code == 201
    assert res.json().get("email") == "user@something.lu"


# TEST login user
def test_login_user(client, create_user):
    res = client.post("/auth", data={"username":create_user["email"],
                                     "password": create_user["password"]})
    assert res.status_code == 200
    assert res.json().get("token_type") == "bearer"
    # Verify the Token data
    payload = jwt.decode(res.json().get("access_token"),
                         jwt_manager.SERVER_KEY, 
                         [jwt_manager.ALGORITHM])
    assert payload["user_id"] == create_user["id"]

# TEST get user by ID
def test_getUseById(authorized_client, create_user):
    res = authorized_client.get(f"/users/{create_user['id']}")
    assert res.status_code == 200
    assert res.json().get("id") == create_user["id"]
    assert res.json().get("email") == create_user["email"]


#### TEST incorrect credentials  #####
@pytest.mark.parametrize("email, password, status_code", [
    (None, "4321", 422),  # No email provided
    ("user@something.lu", None, 422),  # No password provided
    ("wrongemail@gmail.com","4321", 401),  # wrong email
    ("user@something.lu", "wrongpassword", 401), # wrong password
    ("wrongemail@gmail.com", "wrongpassword", 401), # wrong email + wrong password
    ("user@something.lu", "4321", 200) # correct email + correct password
])
def test_credentials_errors(client, create_user, email, password, status_code):
    res = client.post("/auth", data={"username":email,
                                     "password":password})
    assert res.status_code == status_code