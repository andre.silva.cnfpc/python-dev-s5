import pytest
from fastapi.testclient import TestClient
from dbmgt_orm.main import app
from dbmgt_orm.database import get_db
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from dbmgt_orm import models

# Database connexion
DATABASE_URL = "postgresql://postgres:API@localhost:5432/reddit_clone_orm_test"

# Running engine for ORM translation (python to SQL)
database_engine = create_engine(DATABASE_URL)

# Session template for the connection
TestingSessionTemplate = sessionmaker(autocommit=False, autoflush=False, bind=database_engine)

# Dependency to create & close session on-demand
def override_get_db():
    db = TestingSessionTemplate()
    try:
        yield db
    finally:
        db.close()

# requirement session creation with the database
@pytest.fixture()
def session():
    #Clean test DB by deleting the previous table
    models.Base.metadata.drop_all(bind=database_engine)
    #Create the tables
    models.Base.metadata.create_all(bind=database_engine)
    app.dependency_overrides[get_db] = override_get_db


# requirement to run the test client (like postman)
@pytest.fixture()
def client(session):
    yield TestClient(app)

# requirement to run the test client (like postman)
@pytest.fixture()
def client2(session):
    yield TestClient(app)

@pytest.fixture()
def guest_client(session):
    yield TestClient(app)

# requirement to create a user
@pytest.fixture()
def create_user(client):
    user_credentials = {"email":"user@something.lu", "password": "4321"}
    res= client.post("/users", json = user_credentials)
    new_user = res.json()
    # add the passwor the new_user data
    new_user["password"] = user_credentials["password"]
    return new_user

@pytest.fixture()
def user_token(client, create_user):
    res = client.post("/auth", data={"username": create_user["email"],
                                     "password": create_user["password"]})
    return res.json().get("access_token")

@pytest.fixture()
def authorized_client(client, user_token):
    client.headers = { **client.headers, "Authorization": f"bearer {user_token}"}
    yield client

@pytest.fixture()
def create_post(authorized_client):
    return authorized_client.post("/posts", json={"title":"Test title","content":"Test content"}).json()


# requirement to create a user
@pytest.fixture()
def create_user2(client2):
    user_credentials = {"email":"user2@something.lu", "password": "54321"}
    res= client2.post("/users", json = user_credentials)
    new_user = res.json()
    # add the passwor the new_user data
    new_user["password"] = user_credentials["password"]
    return new_user

@pytest.fixture()
def user_token2(client2, create_user2):
    res = client2.post("/auth", data={"username": create_user2["email"],
                                     "password": create_user2["password"]})
    return res.json().get("access_token")

@pytest.fixture()
def authorized_client2(client2, user_token2):
    client2.headers = { **client2.headers, "Authorization": f"bearer {user_token2}"}
    yield client2