import pytest


@pytest.mark.parametrize("a, b, c", [
    (1,2,3),
    (4,5,9),
    (-1,1,0),
    (0,0,0)
])
def test_101(a, b, c):
    print("testing 101")
    assert a + b == c
