
# Testing the creation of a post
def test_create_post(create_user, authorized_client):
    res = authorized_client.post("/posts", json={"title":"Test title",
                                                 "content":"Test content"})
    assert res.status_code == 201
    assert res.json().get("title") == "Test title"
    assert res.json().get("content") == "Test content"
    assert res.json().get("writer").get("email") == "user@something.lu"

# Testing get a post
def test_getPostByID(authorized_client, create_post):
    res = authorized_client.get(f"/posts/{create_post['id']}")
    assert res.status_code == 200
    assert res.json().get("title") == create_post["title"]

# Testing get all posts
def test_getPosts(authorized_client, create_post):
    res= authorized_client.get("/posts")
    assert res.status_code == 200
    assert len(res.json()) > 0

# Testig the update of a blogpost
def test_updatePost(authorized_client, create_post):
    res= authorized_client.put(f"/posts/{create_post['id']}", json={"title":"updated title","content":"updated content", "published": False})
    assert res.status_code == 200
    assert res.json().get("title") == "updated title"
    assert res.json().get("content") == "updated content"

# Testing the deletion of a blogpost
def test_deletePost(authorized_client, create_post):
    res= authorized_client.delete(f"/posts/{create_post['id']}")
    assert res.status_code == 204

# Testing deletion if user isn't the owner
def test_deleteNotPostOwner(authorized_client2, create_post, create_user, create_user2):
    res = authorized_client2.delete(f"/posts/{create_post['id']}")
    assert res.status_code == 403

#
def test_permissionGetPosts(guest_client, create_post):
    res = guest_client.get("/posts")
    assert res.status_code == 401