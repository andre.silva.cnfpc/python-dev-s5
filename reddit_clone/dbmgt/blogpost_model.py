from pydantic import BaseModel
from typing import Optional
import datetime


class BlogPost(BaseModel):
    #id : int = None
    title: str
    content: str
    published : bool = True
    #created_at : Optional[datetime] = None
    rating: Optional[int] = None