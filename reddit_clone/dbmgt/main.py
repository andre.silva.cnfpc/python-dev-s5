from fastapi import FastAPI, HTTPException, Body, status
from .database import cursor,connection
from .blogpost_model import BlogPost

app = FastAPI()

#get all posts
@app.get("/posts")
def get_posts():
    cursor.execute("Select * from blogpost")
    database_posts = cursor.fetchall()
    return {"blog posts" : database_posts}


@app.get("/posts/{id_uri}")
def get_post(id_uri : int):
    cursor.execute("SELECT * FROM blogpost where id=%i" % int(id_uri))
    my_post = cursor.fetchone()
    if not my_post:
        raise HTTPException (
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"No post with id {id_uri} was found."
        )
    return {"post" : my_post}

@app.post("/posts")
def insert_post(the_post: BlogPost):
    sql_query = f"insert into blogpost (title,content,published) values ('{the_post.title}','{the_post.content}',{the_post.published}) returning *;"
    cursor.execute(sql_query) #execute the query
    new_post = cursor.fetchone() #retrieve the result of the query
    connection.commit() #write the changes into the database
    return {"new post" : new_post} #show the results to the user

@app.put("/posts/{post_id}")
def update_post(post_id : int, the_post : BlogPost):
    sql_query = f"update blogpost set title = '{the_post.title}', content = '{the_post.content}', published={the_post.published}, rating = {the_post.rating} where id = {post_id} returning *;"
    print(sql_query)
    cursor.execute(sql_query)
    upd_post = cursor.fetchone()
    if not upd_post:
        raise HTTPException (
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"No post with id {post_id} was found."
        )
    connection.commit()
    return {"updated post" : upd_post}

@app.delete("/posts/{id_uri}")
def get_delete(id_uri : int):
    cursor.execute("Delete FROM blogpost where id=%i returning *;" % int(id_uri))
    my_post = cursor.fetchone()
    if not my_post:
        raise HTTPException (
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"No post with id {id_uri} was found."
        )
    return {"deleted post" : my_post}
