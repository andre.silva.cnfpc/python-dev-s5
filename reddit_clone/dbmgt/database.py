import psycopg2
from psycopg2.extras import RealDictCursor
from time import sleep

while True:
    try:
        connection = psycopg2.connect(
            host = 'localhost',
            port = 5432,
            database = 'dbmgt',
            user = 'postgres',
            password = 'API',
            cursor_factory = RealDictCursor
        )
        cursor = connection.cursor()
        print('Database connection: successful')
        break
    except Exception as error:
        print('Database connection: failed')
        print('Errors: ', error)
        sleep(2)
    