from .database import database_engine
from fastapi import FastAPI
from .routers import posts_router, users_router, auth_router
from . import models
from fastapi.middleware.cors import CORSMiddleware


# Create the tables if they don't exist yet
models.Base.metadata.create_all(bind=database_engine)

# Run the server
app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins = ["*"],
    allow_credentials = True,
    allow_methods = ["*"],
    allow_headers = ["*"]
)

# Add routers
app.include_router(posts_router.router)
app.include_router(users_router.router)
app.include_router(auth_router.router)
