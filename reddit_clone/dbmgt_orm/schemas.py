from datetime import datetime
from pydantic import BaseModel, EmailStr

# User pydantic schema request
# Data parsing & validation of /users "Body"
class User_request(BaseModel):
    email: EmailStr # str is too general (random text)
    password: str

# User credentials schema request based on User_request
class User_credentials(User_request):
    pass # pass means we want the same fields

# User pydantic schema reponse
class User_response(User_request):
    id: int
    created_at: datetime
    class Config: # Important for pydantic schema translation
        # orm_mode = True
        # v2 'orm_mode' has been renamed to 'from_attributes'
        from_attributes = True

# Blogpost pydantic schema request
# Data parsing & validation of /posts "Body"
class Blogpost_request(BaseModel):
    title: str
    content: str
    published: bool = True
    # writer_id: int # coming from JWT Token

# Blogpost pydantic schema reponse
class Blogpost_response(Blogpost_request):
    id: int
    created_at: datetime
    writer: User_response
    class Config: # Important for pydantic schema translation
        # orm_mode = True
        # v2 'orm_mode' has been renamed to 'from_attributes'
        from_attributes = True

class Token(BaseModel):
    access_token: str
    token_type: str