from fastapi import APIRouter, Depends, HTTPException, status, Response
from typing import List
from .. import models, schemas
from ..database import get_db
from sqlalchemy.orm import Session
from ..utilities import hash_manager, jwt_manager

router = APIRouter(prefix="/users", tags= ["users"])

###############
# GET /users
###############
@router.get("/", response_model=List[schemas.User_response])
def get_allusers(db: Session = Depends(get_db), 
                user_id:int = Depends(jwt_manager.decode_token)):

    # sql_query=f"SELECT * FROM \"user\""
    database_users = db.query(models.User).all() # models

    return database_users


###############
# GET /users/id
###############
@router.get('/{id_uri}', response_model=schemas.User_response)
def get_user(id_uri: int, db: Session = Depends(get_db), 
                user_id:int = Depends(jwt_manager.decode_token)):
    # sql_query=f"SELECT * FROM \"user\" WHERE id={id_uri}"
    corresponding_user = db.query(models.User).filter(models.User.id == id_uri).first()
    if not corresponding_user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding user was found with id: {id_uri}"
        )
    return corresponding_user

###############
# POST /users
# Path operation function to Create a User
###############
@router.post('/', response_model=schemas.User_response, status_code=status.HTTP_201_CREATED)
def create_user(user_body: schemas.User_request, db: Session = Depends(get_db)):
    # Create user using the data sent by the client (Same as for the blogposts)
    new_user = models.User(**user_body.model_dump())
    # hash password before sending to the DB
    new_user.password = hash_manager.hash_pass(user_body.password)
    db.add(new_user) #send the query
    db.commit() #Save the staged changes to the DB
    db.refresh(new_user) # to replace "RETURNING *"
    return new_user


###############
# DELETE /users/{id_uri}
# Objective: Delete the user and all its blogposts
###############
@router.delete('/{id_uri}')
def delete_user(id_uri:int, db: Session = Depends(get_db), 
                user_id:int = Depends(jwt_manager.decode_token)):

    deleting_user = db.query(models.User).filter(models.User.id == id_uri)

    if not deleting_user.first(): # Check if the user exists
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Not corresponding user was found with id: {id_uri}"
                            )
    
    deleting_allpost = db.query(models.Blogpost).filter(models.Blogpost.writer_id==id_uri)
    deleting_allpost.delete() # Delete all blogposts of the user

    deleting_user.delete() # Delete the user
    db.commit() # Save changes into database

    return Response(status_code=status.HTTP_204_NO_CONTENT)