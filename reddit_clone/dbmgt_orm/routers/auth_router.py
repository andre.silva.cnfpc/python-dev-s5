from fastapi import APIRouter, Depends, HTTPException, status
from .. import schemas, models
from ..database import get_db
from sqlalchemy.orm import Session
from ..utilities import hash_manager, jwt_manager
from fastapi.security.oauth2 import OAuth2PasswordRequestForm


router = APIRouter(prefix="/auth", tags=["auth"])


wrongCredentialsException = HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate":"bearer"}
        ) 
#####
# POST /auth
# path operation /auth
#####
@router.post("/", response_model=schemas.Token)
def auth_user(user_credentials: OAuth2PasswordRequestForm = Depends(), 
              db: Session = Depends(get_db)):
    # Query database for corresponding user
    corresponding_user = db.query(models.User).filter(
        models.User.email == user_credentials.username).first()
    # IF NO EMAIL FOUND
    if not corresponding_user:
        raise wrongCredentialsException
    # CHECK THE PASSWORD
    pwd_check = hash_manager.verify_password(
        user_credentials.password,corresponding_user.password)
    # IF incorrect password
    if not pwd_check:
        raise wrongCredentialsException
    # GENERATE NEW TOKEN FOR THE AUTHANTICATED USER
    jwt = jwt_manager.generate_token(corresponding_user.id)
    return jwt