from typing import List
from fastapi import APIRouter, Depends, HTTPException, status, Response
from .. import models, schemas
from ..database import get_db
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
from ..utilities import jwt_manager

router = APIRouter(prefix="/posts", tags=["posts"])

###############
# POST /posts
# Path operation function to Create a blogpost
###############
@router.post('/', response_model=schemas.Blogpost_response, status_code=status.HTTP_201_CREATED)
def create_post(post_body: schemas.Blogpost_request, db: Session = Depends(get_db), 
                user_id:int = Depends(jwt_manager.decode_token)):
    try:
        # Create post using the data sent by the client
        # sql_query=f"INSERT INTO blogpost (title, content, published) VALUES('{post_body.title}', '{post_body.content}’ {post_body.published}) RETURNING *;"
        new_post = models.Blogpost(**post_body.model_dump(), writer_id= user_id)
        db.add(new_post) #send the query
        db.commit() #Save the staged changes to the DB
        db.refresh(new_post) # to replace "RETURNING *"
    except IntegrityError as ie: # equivalent to ForeignKeyViolation of psycopg2.errors
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f"Non-existent or invalid foreign key : {ie}")
    return new_post
    

###############
# GET /posts
# path operation function to get ALL blogposts
###############
@router.get("/", response_model=List[schemas.Blogpost_response])
def get_posts(db: Session = Depends(get_db), 
                user_id:int = Depends(jwt_manager.decode_token)):
    all_posts = db.query(models.Blogpost).all()
    return all_posts


###############
# GET /posts/{uri_id}
# GET BlogPost by ID
###############
@router.get("/{uri_id}", response_model=schemas.Blogpost_response)
def get_post_byID(uri_id:int, db: Session = Depends(get_db), 
                user_id:int = Depends(jwt_manager.decode_token)):
    #send the query
    corresponding_post = db.query(models.Blogpost).filter(models.Blogpost.id == uri_id ).first()
    # Check if correspondig post doesn't exist
    if not corresponding_post:
        raise HTTPException(
            status_code= status.HTTP_404_NOT_FOUND,
            detail = f"Not corresponding post was found with id:{uri_id}"
        )
    return corresponding_post


###############
# DELETE /posts/{uri_id}
# DELETE BlogPost by ID
###############
@router.delete("/{uri_id}")
def delete_post(uri_id: int, db: Session = Depends(get_db), 
                user_id:int = Depends(jwt_manager.decode_token) ):
     #send the query
     corresponding_post_query = db.query(models.Blogpost).filter(models.Blogpost.id == uri_id )
     # Check if correspondig post doesn't exist
     if not corresponding_post_query.first():
        raise HTTPException(
            status_code= status.HTTP_404_NOT_FOUND,
            detail = f"Not corresponding post was found with id:{uri_id}"
        )
     # Checking if user is the author of this post
     if corresponding_post_query.first().writer_id != user_id:
         raise HTTPException(
             status_code= status.HTTP_403_FORBIDDEN,
             detail = "Your are not the author of this post!"
         )
     corresponding_post_query.delete() #Delete the post
     db.commit() #Save changes to DB
     return Response(status_code=status.HTTP_204_NO_CONTENT)


###############
# PUT /posts/{uri_id}
# UPDATE BLogPost by ID
###############
@router.put("/{uri_id}", response_model=schemas.Blogpost_response)
def update_post(uri_id: int, post_body:schemas.Blogpost_request, db: Session = Depends(get_db), 
                user_id:int = Depends(jwt_manager.decode_token)):
    query_post = db.query(models.Blogpost).filter(models.Blogpost.id == uri_id)
    # Check if correspondig post doesn't exist
    if not query_post.first():
        raise HTTPException(
            status_code= status.HTTP_404_NOT_FOUND,
            detail = f"Not corresponding post was found with id:{uri_id}"
        )
    # Checking if user is the author of this post
    if query_post.first().writer_id != user_id:
        raise HTTPException(
            status_code= status.HTTP_403_FORBIDDEN,
            detail = "Your are not the author of this post!"
        )
    query_post.update(post_body.model_dump()) #Update the posts
    db.commit() #Save changes to db
    return query_post.first()
