from fastapi import Depends, HTTPException, status
from jose import JWTError, jwt, ExpiredSignatureError
from ..schemas import Token
from fastapi.security import OAuth2PasswordBearer
from datetime import datetime, timedelta
from ..config import settings

oauth2_scheme = OAuth2PasswordBearer("/auth")

# Secret Key to sign JWT Tokens
# openssl rand -hex 32
SERVER_KEY = settings.server_key
ALGORITHM = settings.algorithm
EXPIRATION_MINUTES = settings.expiration_minutes

def generate_token(user_id: int):
    payload = {"user_id": user_id,
               "exp": datetime.utcnow() + timedelta(minutes=EXPIRATION_MINUTES)}
    encoded_jwt = jwt.encode(payload, SERVER_KEY, algorithm=ALGORITHM)
    return Token(access_token=encoded_jwt, token_type="bearer")

def decode_token(provided_token : str = Depends(oauth2_scheme)):
    try:
        payload = jwt.decode(provided_token, SERVER_KEY, algorithms=[ALGORITHM])
        decoded_id = payload["user_id"]
    except ExpiredSignatureError:
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail="JWT Token Expired",
            headers={"WWW-Authenticate":"bearer"}
        ) 
    except JWTError:
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate your credentials",
            headers={"WWW-Authenticate":"bearer"}
        )     
    return decoded_id