from passlib.context import CryptContext

# Initialize the CryptContext
pwd_context = CryptContext(schemes=["bcrypt"])


def hash_pass(password:str):
    return pwd_context.hash(password)

def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password,hashed_password)
