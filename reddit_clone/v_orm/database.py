# Used for everything related to connection to the DB
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# database URL
DATABASE_URL = "postgresql://postgres:API@localhost/reddit_clone_orm"

# create the engine for ORM translation (python to SQL)
database_engine = create_engine(DATABASE_URL)

# template of the session for the connection
SessionTemplate = sessionmaker(
    autocommit=False, autoflush=False, bind=database_engine )

# Create and close sessions on-demand
def get_db():
    db = SessionTemplate()
    try:
        yield db
    finally:
        db.close()