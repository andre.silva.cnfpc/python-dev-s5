# Contains our python classes that interact with the 
from sqlalchemy import Column, Integer, String, Boolean, TIMESTAMP, text
from sqlalchemy.ext.declarative import declarative_base
#from sqlalchemy.sql.sqltypes import TIMESTAMP
#from sqlalchemy.sql.expression import text

# The base class will be the base for all the models we'll create
Base = declarative_base()


# Class / Model / Table
class BlogPost(Base):
    __tablename__ = "BlogPost"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, nullable=False)
    content = Column(String, nullable = False)
    published = Column(Boolean, nullable=False, server_default='TRUE')
    created_at = Column(TIMESTAMP(timezone=True),
                        nullable=False, server_default=text('now()')) 