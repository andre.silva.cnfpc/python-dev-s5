from fastapi import Depends, FastAPI, HTTPException, Response, status
from v_orm.schemas import BlogPostPy
from . import models
from .database import database_engine, get_db
from sqlalchemy.orm import Session

#Create the tables if they don't exist yet
models.Base.metadata.create_all(bind = database_engine)

#Instantiate FastAPI Server
app = FastAPI()

# path operation function to get ALL blogposts
@app.get("/posts")
def get_posts(db: Session = Depends(get_db)):
    all_posts = db.query(models.BlogPost).all()
    return all_posts

#Path operation function to Create a blogpost
@app.post("/posts", status_code=status.HTTP_201_CREATED)
def create_post(request_body:BlogPostPy, db: Session = Depends(get_db)):
    # Create post using the data sent by the client
    created_post = models.BlogPost(**request_body.model_dump())
    db.add(created_post) #send the query
    db.commit() #Save the staged changes to the DB
    db.refresh(created_post) # to replace "RETURNING *"
    return created_post

# GET BlogPost by ID
@app.get("/posts/{uri_id}")
def get_post_byID(uri_id:int, db: Session = Depends(get_db)):
    #send the query
    corresponding_post = db.query(models.BlogPost).filter(models.BlogPost.id == uri_id ).first()
    # Check if correspondig post doesn't exist
    if not corresponding_post:
        raise HTTPException(
            status_code= status.HTTP_404_NOT_FOUND,
            detail = f"Not corresponding post was found with id:{uri_id}"
        )
    return corresponding_post

# DELETE BlogPost by ID
@app.delete("/posts/{uri_id}")
def delete_post(uri_id: int, db: Session = Depends(get_db) ):
     #send the query
     corresponding_post_query = db.query(models.BlogPost).filter(models.BlogPost.id == uri_id )
     # Check if correspondig post doesn't exist
     if not corresponding_post_query.first():
        raise HTTPException(
            status_code= status.HTTP_404_NOT_FOUND,
            detail = f"Not corresponding post was found with id:{uri_id}"
        )
     corresponding_post_query.delete() #Delete the post
     db.commit() #Save changes to DB
     return Response(status_code=status.HTTP_204_NO_CONTENT)

# UPDATE BLogPost by ID
@app.put("/posts/{uri_id}")
def update_post(uri_id: int, post_body:BlogPostPy, db: Session = Depends(get_db) ):
    query_post = db.query(models.BlogPost).filter(models.BlogPost.id == uri_id)
    # Check if correspondig post doesn't exist
    if not query_post.first():
        raise HTTPException(
            status_code= status.HTTP_404_NOT_FOUND,
            detail = f"Not corresponding post was found with id:{uri_id}"
        ) 
    query_post.update(post_body.model_dump()) #Update the posts
    db.commit() #Save changes to db
    return query_post.first()
