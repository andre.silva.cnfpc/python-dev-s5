# Validate request & response body (data) using pydantic

from pydantic import BaseModel

#Pydantic schema for POST Body Validation
class BlogPostPy(BaseModel):
    title: str
    content: str
    published: bool = True

