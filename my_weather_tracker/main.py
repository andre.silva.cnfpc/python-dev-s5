import json
import requests

# API base URL
BASE_URL = "http://api.weatherstack.com/current"

# API Key for Weatherstack
API_KEY = "56993542c8363a4b75130708e485c04a"

# File name for storing the city list
FILE_NAME = "city_list.json"


def load_city_list():
    #Load the list of cities from the JSON file.
    with open(FILE_NAME, 'r') as f:
        data = json.load(f)
    return data["cities"]


def save_city_list(cities):
    #saves the updated list of cities to the JSON file
    with open(FILE_NAME, 'w') as f:
        json.dump({"cities": cities}, f)


def add_city():
    #Add a new city to the list.
    city = input("Enter the city name: ")
    cities = load_city_list()
    cities.append(city)
    save_city_list(cities)
    print(f"{city} added successfully!\n")


def remove_city():
    #Remove a city from the list.
    city = input("Enter the city name to remove: ")
    cities = load_city_list()
    if city in cities:
        cities.remove(city)
        save_city_list(cities)
        print(f"{city} removed successfully!\n")
    else:
        print(f"{city} is not in the list!\n")


def view_city_list():
    #Display the list of tracked cities.
    cities = load_city_list()
    if cities:
        print("Tracked Cities:")
        for city in cities:
            print(city)
        print()
    else:
        print("No cities are being tracked.\n")


def view_weather():
    #Fetch and display weather data for each tracked city.
    cities = load_city_list()
    for city in cities:
        response = requests.get(BASE_URL, params={
            "access_key": API_KEY,
            "query": city
        })
        if response.status_code == 200:
            data = response.json()
            if "error" in data:
                print(f"Failed to retrieve weather data for {city}! Error: {data['error']['info']}\n")
            else:
                print(f"{city}: {data['current']['temperature']}°C, {data['current']['weather_descriptions'][0]}\n")
        else:
            print(f"HTTP Error {response.status_code} occurred while retrieving weather data for {city}!\n")


while True:
    print("Welcome to the Simple Weather Tracker!")
    print("1. Add a City")
    print("2. Remove a City")
    print("3. View City List")
    print("4. View Weather for Cities")
    print("5. Exit")
    
    option = input("Choose an option: ")
    
    if option == "1":
        add_city()
    elif option == "2":
        remove_city()
    elif option == "3":
        view_city_list()
    elif option == "4":
        view_weather()
    elif option == "5":
        print("Exiting the application. Goodbye!")
        break
    else:
        print("Invalid option! Please choose a valid option.\n")