import streamlit as st
import pandas as pd


# Sidebar
with st.sidebar:
    st.write("Text in the sidebar")
    st.text_input("Username", type="default")
    st.text_input("Password", type="password")

#Columns
col1, col2, col3 = st.columns(3)
col1.write("Text in a column")
slider = col2.slider("Choose a number")
col3.write(slider)

#Tabs
df = pd.read_csv("data/financial_data.csv")

tab1, tab2 = st.tabs(["Line plot", "Area plot"])

tab1.write("A line plot")
tab1.line_chart(df, x="Month", y=["Sales","Expenses","Profit"])


tab2.write("A area plot")
tab2.area_chart(df, x="Month", y=["Sales","Expenses","Profit"])

# Expander (collapsible element)
expander = st.expander("Click to expand")
expander.write("This is a text")
expander.area_chart(df, x="Month", y=["Sales","Expenses","Profit"])
