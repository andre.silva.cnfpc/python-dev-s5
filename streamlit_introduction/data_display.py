import streamlit as st
import pandas as pd

df = pd.read_csv("data/financial_data.csv")

st.title("Displaying Data using Streamlit")

st.subheader("As DataFrame")
st.dataframe(df)

st.subheader("Alternative using st.write")
st.write(df)

st.subheader("As static table")
st.table(df)

st.subheader('Displaying Metrics') 
st.metric(label="Expenses", value=900, delta=20, delta_color="normal")
st.metric(label="Expenses", value=900, delta="-20", delta_color="normal") 

st.subheader("line chart")
st.line_chart(df, x="Month", y=["Sales","Expenses","Profit"])

st.subheader("area chart")
st.area_chart(df, x="Month", y=["Sales","Expenses","Profit"])

st.subheader("bar chart")
st.bar_chart(df, x="Month", y=["Sales","Expenses","Profit"])

st.subheader("Displaying Maps")
geo_df = pd.read_csv("data/sample_map.csv")

st.map(geo_df)

## display image 
st.subheader("Image")
st.image("./data/img_greece.jpg")

## display video
st.video("https://www.youtube.com/watch?v=jfKfPfyJRdk")




