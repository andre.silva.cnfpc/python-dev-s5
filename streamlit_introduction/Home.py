import streamlit as st
import pandas as pd

#Set page configuration
st.set_page_config(
    page_title="Homepage",
    page_icon="🏡",
    layout= "centered",
    initial_sidebar_state="expanded"
)

st.title("Homepage")

#Initialize a sample df and store it in the session state
df = pd.DataFrame({"column1": [1,2,3],
                   "column2": [4,5,6]})

if "df" not in st.session_state:
    st.session_state["df"] = df

st.write(st.session_state)


# Init permanent keys
for key in ["product", "x1", "x2"]:
    if key not in st.session_state:
        st.session_state[key] = 0

# Define Callback functions used for maintaining state consistency
# copy keys from temp to permanent
def keep(key):
    st.session_state[key] = st.session_state["_"+key]

# function to reassign value to temp key
# copy from permament to temporary
def unkeep(key):
    st.session_state["_"+key] = st.session_state[key]


def multiply(x1,x2):
    st.session_state["product"] = x1 * x2

col1, col2 = st.columns(2)
unkeep("x1")
x1 = col1.number_input("Pick a number", 
                       key="_x1",
                       on_change=keep,
                       args=(("x1",)))
unkeep("x2")
x2 = col2.number_input("Pick another number",
                       key="_x2",
                       on_change=keep,
                       args=(("x2",)))
st.button("Multiply!", type="primary", on_click=multiply, args=((x1, x2)))