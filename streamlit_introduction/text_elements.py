import streamlit as st

#Give your app a title
st.title("Your title")

#Header
st.header("Main header")

#subheader
st.subheader("Sub header")

#Markdown
st.markdown("**This is bold text**")
st.markdown("# Heading1")
st.markdown("## Heading2")
st.markdown("### Heading3")

# caption
st.caption("this is a caption")

# code block 
st.code("""import pandas as pd
pd.read_csv(my_csv_file)""")

st.code("import pandas as pd\npd.read_csv(my_csv_file)")

#raw text
st.text("This is some raw text")

#LaTex formatting
st.latex("x= 2^2")

#Divider
st.text("Text above divider")
st.divider()
st.text("Text below divider")

# st.write can display a lot of things eg. images, texts...
st.write("this some text using st.write")

st.balloons()

