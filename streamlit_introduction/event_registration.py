import streamlit as st

st.title("Event Registration Form")

# Create a new form
with st.form("event_registration_form"):
    st.write("Event Registration")
    
    # Dropdown for event type
    event_type = st.selectbox("Event Type", ["Workshop", "Seminar", "Networking"])
    
    # Dropdown for number of attendees
    attendees = st.selectbox("Number of Attendees", [1, 2, 3, 4, 5])
    
    # Checkbox for special accommodations
    special_accommodations = st.checkbox("Request Special Accommodations")
    
    # Date input for event date
    event_date = st.date_input("Event Date")
    
    # Time input for event time
    event_time = st.time_input("Event Time")
    
    # Text area for additional notes
    additional_notes = st.text_area("Additional Notes")
    
    # Submit button
    submit_button = st.form_submit_button("Register")

    # Display the summary of the form upon submission
    if submit_button:
        st.write(f"Event Type: {event_type}")
        st.write(f"Number of Attendees: {attendees}")
        st.write(f"Special Accommodations: {'Yes' if special_accommodations else 'No'}")
        st.write(f"Event Date: {event_date}")
        st.write(f"Event Time: {event_time}")
        st.write(f"Additional Notes: {additional_notes}")
