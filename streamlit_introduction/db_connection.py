import streamlit as st

#Initialize connection
conn = st.connection("postgresql", type="sql")

#Perform query
df = conn.query('SELECT * FROM "public"."user" LIMIT 100', ttl="10m")

st.write(df)