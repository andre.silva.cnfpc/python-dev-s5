import streamlit as st
import pandas as pd

st.title("Input Widgets")

# Buttons
st.subheader("Buttons")
primary_btn = st.button(label="Primary", type="primary")
primary_btn2 = st.button(label="Primary2", type="primary")
secondary_btn = st.button(label="Secondary", type="secondary")

if primary_btn:
    st.write("Hello from primary")
    st.balloons()

if primary_btn2:
    st.write("Hello from primary2")
    st.warning("⚠ Don't click primary2")

if secondary_btn:
    st.write("Hello from secondary")
    st.snow()

# Checkbox
st.subheader("Checkbox")

checkbox = st.checkbox("Remember me")

if checkbox:
    st.write("I will remember you")
else: 
    "I will forget you"

# Radio buttons
st.subheader("Radio buttons")

df = pd.read_csv("data/financial_data.csv")

radio = st.radio("Choose a column", 
                 options= df.columns[1:], horizontal=True, index=1)

# Select box
st.subheader("Selectbox")
select = st.selectbox("Choose a column", options=df.columns[1:])
select

# Multi-selection Widget
st.subheader("Multiselect")
multiselect = st.multiselect("Choose as many columns as you want", 
                             options=df.columns[1:], default=["Sales"], 
                             max_selections=2)
st.write(multiselect)
st.write(":smile: 😄 ")
st.write(":rainbow[André🤣]")

#
column = st.columns(5)
for i in range(5):
    with column[i]:
        num = st.checkbox(f"text{i}")
        if num:
            st.markdown(f"this is {i}")

# Slider widget
st.subheader("Slider")
slider = st.slider("Pick a number", 
                   min_value=0.1, 
                   max_value=9.9, 
                   value=4.9,
                   step=0.1)

# Text input widget
st.subheader("Text input")
text_input = st.text_input("What's your name?", placeholder="Enter your name" )

# Number
st.subheader("Number input")
num_input = st.number_input("Pick a number", min_value=1, max_value=49, step=1)

# Text Area widget
st.subheader("Text Area")
txt_area = st.text_area("Blogpost content:", 
                        height=500, 
                        placeholder="content goes here...")

# Toogle
st.subheader("Toogle")
st.toggle("Dark mode", value=True)