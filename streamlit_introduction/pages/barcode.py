import cv2
from pyzbar.pyzbar import decode
import streamlit as st
from PIL import Image

def detect_barcode(image_path):
    # Load the image
    img = cv2.imread(image_path)

    # Convert to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Detect barcodes
    barcodes = decode(gray)

    # Extract barcode information
    if barcodes:
        barcode_data = barcodes[0].data.decode("utf-8")
        return barcode_data
    else:
        return "No barcode detected"

st.title("Barcode Scanner App")

# Capture image from camera
image = st.camera_input("Capture Barcode")

if image:
    # Convert the UploadedFile to a PIL Image
    pil_image = Image.open(image)

    # Save the PIL Image temporarily
    temp_image_path = "temp_image.png"
    pil_image.save(temp_image_path)

    # Detect barcode
    barcode_info = detect_barcode(temp_image_path)

    # Display the barcode information
    st.write(f"Detected barcode: {barcode_info}")
