import streamlit as st

st.title("Contacts")

st.table(st.session_state["df"])

st.write(st.session_state)

x1 = st.session_state["x1"]
x2 = st.session_state["x2"]
st.write(f"You chose to multiply {x1} with {x2}")
st.write("Check the second page for the result!")