import streamlit as st

st.title("Sateful app")

st.write("Here is the current session state:")
st.write(st.session_state)

st.button("Update state")

st.write("setting key and value")
#Set value using the key-value syntax
if "key" not in st.session_state:
    st.session_state["key"] = "value"

st.write("setting attribute and another value")
#Set the value using the attribute syntax
if "attribute" not in st.session_state:
    st.session_state.attribute = "another value"

#Read value from session state
st.write(f"Reading key-value from session state: {st.session_state['attribute']}")

st.write("Here is the current session state:")
st.write(st.session_state)

#Delete item from state
st.write("Delete key and value")
del st.session_state["key"]

st.write("Here is the current session state:")
st.write(st.session_state)



