import streamlit as st
import requests
import json

# API base URL and Key
BASE_URL = "http://api.weatherstack.com/current"
API_KEY = "0fb1299cd81ce9f22b9f17ed2f3bb21b"  # Use your actual API key here

# File name for storing and loading the city list
FILE_NAME = "city_list.json"

#Load the list of cities from a JSON file.
def load_city_list():
    try:
        with open(FILE_NAME, 'r') as f:
            data = json.load(f)
        return data["cities"]
    except FileNotFoundError:
        return []

#Save the updated list of cities to a JSON file.
def save_city_list(cities):
    with open(FILE_NAME, 'w') as f:
        json.dump({"cities": cities}, f)

#Fetch weather data for a given city.
def fetch_weather(city):
    response = requests.get(BASE_URL, params={"access_key": API_KEY, "query": city})
    if response.status_code == 200:
        data = response.json()
        if "error" in data:
            return f"Error: {data['error']['info']}"
        else:
            return f"{city}: {data['current']['temperature']}°C, {data['current']['weather_descriptions'][0]}"
    else:
        return f"HTTP Error {response.status_code}"
    
#Fetch location data for a given city
def fetch_location(city):
    response = requests.get(BASE_URL, params={"access_key": API_KEY, "query": city})
    if response.status_code == 200:
        data = response.json()
        if "error" in data:
            return f"Error: {data['error']['info']}"
        else:
            return data['location']['lat'], data['location']['lon']
    else:
        return f"HTTP Error {response.status_code}"