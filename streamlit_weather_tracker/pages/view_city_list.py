import streamlit as st
import pandas as pd
from utils import load_city_list, fetch_location

st.header("View City List")

cities = load_city_list()
if cities:
    st.write("Tracked Cities:")
    st.table(cities)
else:
    st.write("No cities are being tracked.")

#Initialize an emty list to store location data
locations = []

for city in cities:
    lat, lon = fetch_location(city)
    try:
        locations.append([float(lat), float(lon)])
    except ValueError:
        st.write(f"Error converting lat/lon to float for city {city}: lat={lat}, lon={lon}")

#Convert the list to a DataFrame
locations_df = pd.DataFrame(locations, columns=["latitude","longitude"])

st.map(locations_df)