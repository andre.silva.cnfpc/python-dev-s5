import streamlit as st
from utils import load_city_list, fetch_weather

st.header("View Weather for Cities")

cities = load_city_list()
if cities:
    for city in cities:
        city_weather = fetch_weather(city)
        st.write(city_weather)
else:
    st.write("No cities are being tracked.")
