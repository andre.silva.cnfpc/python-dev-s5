import streamlit as st
from utils import load_city_list, save_city_list  # Assume these functions are defined in a separate utility file (utils.py)

st.header("Add a City")

city = st.text_input("Enter the city name:")
if st.button("Add City"):
    if city:
        cities = load_city_list()
        cities.append(city)
        save_city_list(cities)
        st.success(f"{city} added successfully!")
    else:
        st.error("Please enter a city name.")
