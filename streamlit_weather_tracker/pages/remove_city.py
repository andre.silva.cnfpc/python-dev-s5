import streamlit as st
from utils import load_city_list, save_city_list  # Assume these functions are defined in a separate utility file (utils.py)

st.header("Remove a City")

cities = load_city_list()
city = st.selectbox("Select a city to remove:", cities)
if st.button("Remove City"):
    if city in cities:
        cities.remove(city)
        save_city_list(cities)
        st.success(f"{city} removed successfully!")
    else:
        st.error("City not found.")
