import streamlit as st

# Main page content
st.title("My Weather Tracker")
st.write("""
Welcome to the My Weather Tracker app! Navigate through the sidebar to access different functionalities:
- Add a City
- Remove a City
- View City List
- View Weather for Cities

Enjoy tracking the weather in your favorite cities!
""")
