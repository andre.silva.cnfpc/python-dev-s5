import requests

BASE_URL = "https://python-dev-s5.onrender.com"

def authenticate(username: str, password: str):
    #Authenticate with the backend and get a token
    try:
        #POST request to /auth endpoint
        res= requests.post(f"{BASE_URL}/auth/", data={"username": username, "password": password})
        return res.json()["access_token"]
    except Exception as e :
        print(f"Authentication failed {e}")
        return None
    
def get_all_users(token: str):
    try:
        res = requests.get(f"{BASE_URL}/users/", 
                           headers={"Authorization":f"bearer {token}"})
        return res.json()
    except Exception as e :
        print(f"Failed to fetch users {e}")