import streamlit as st
from api_client import authenticate, get_all_users


st.title("Reddit Clone Web Client")

#Sidebar section for user authentication
st.sidebar.header("Authentication")
username = st.sidebar.text_input("Username")
password = st.sidebar.text_input("Password", type="password")

#Login button 
if st.sidebar.button("Login"):
    # Attempt to authenticate
    token = authenticate(username, password)
    if token:
        # If authentication is successful, store the token in session state
        st.session_state["auth_token"] = token
        st.sidebar.success("Logged in successfully")
    else:
        st.sidebar.error("Failed to login")

        if token not in st.session_state:
            st.session_state["auth_token"] = token


if "auth_token" in st.session_state:
    token = st.session_state["auth_token"]
    st.subheader("All the users")
    if st.button("Show All Users"):
        all_users = get_all_users(token)
        st.dataframe(all_users)
else:
    st.warning("Please log in to see the content.")


